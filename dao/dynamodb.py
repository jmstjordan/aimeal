
VEGETARIAN = 'Vegetarian'
VEGAN = 'Vegan'
CARNIVORE = 'Carnivore'

def get_user_data(user_id):
    """
    Given a unique user_id (string) access dynamodb
    and retrieve the associated json object
    :param user_id: 
    :return: json user data
    """
    user = {
        'UserId': '1',
        'Seed': {
            'Palate': VEGETARIAN,
            'DietLabels': [
                'shellfish-free'
            ]
        },
        'RecipeSuggestions': [

        ],
        'PreferenceHistory': {

        },
        'Inventory': [
            'steak',
            'kale',
            'eggs',
            'bread',
            'cheese'
        ]
    }

    return user


def get_all_user_data():
    """
    necessary for a refresh of all the user data with 
    new model information.
    :return: list of json user data objects
    Retrieves all user data from dynamodb. This is
    """
    return None