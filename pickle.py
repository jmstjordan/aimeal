import api.edemam as api
import pickle
import models.Recipe as Recipe
import json


def pickle_keyword(keyword, **kwargs):
    """

    THIS FUNCTION IS NOT FINISHED

    Takes in a keyword string pickles it as a list 
    of Recipes to disk
    :param keyword: 
    :return: 
    """
    recipe_list = json.loads(api.search('q', keyword, **kwargs).text)
    for hit in recipe_list['hits']:
        recipe = hit.recipe
        new_recipe = Recipe(
            edemam_id=recipe.uri,
            name=recipe.label,
            ingredients=recipe.ingredientLines,
            instructions=recipe.url,
            diet_labels=recipe.dietLabels,
            health_labels=recipe.healthLabels,
            cautions=recipe.cautions,
            calories=recipe.calories,
            nutrients=recipe.totalNutrients,
            image=recipe.image
        )
        with open('pickle.dat', "rb") as f:
            pickle.dump(new_recipe, f)


def load_pickle():
    """
    Loads the most recent pickled recipe(s)
    :return: 
    """
    with open('pickle.dat', "rb") as f:
        return pickle.load(f)


pickle_keyword('beets', diet='low-fat')
print(load_pickle())