## Title
AiMeal

## Installation Instructions
After cloning this repo, install anaconda- a python environment

https://conda.io/docs/using/envs.html

Then create a new environment from this one:

    $ conda env create -f environment.yml

Then, activate your new environment, activate it with:

    $ source activate aimeal

Now, when you install a new package, use:

    $ conda install <package_name>

Here's the api documentation we are using. We aren't using foodtofork.py right now.

https://developer.edamam.com/edamam-docs-recipe-api

