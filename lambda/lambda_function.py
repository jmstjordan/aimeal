"""
This sample demonstrates a simple skill built with the Amazon Alexa Skills Kit.
The Intent Schema, Custom Slots, and Sample Utterances for this skill, as well
as testing instructions are located at http://amzn.to/1LzFrj6

For additional samples, visit the Alexa Skills Kit Getting Started guide at
http://amzn.to/1LGWsLG
"""

from __future__ import print_function
import random
import boto3


# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': "SessionSpeechlet - " + title,
            'content': "SessionSpeechlet - " + output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the Alexa Skills Kit sample. " \
                    "Please tell me your favorite meal by saying, " \
                    "my favorite meal is red"
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "Please tell me your favorite meal by saying, " \
                    "my favorite meal is red."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for trying the Alexa Skills Kit sample. " \
                    "Have a nice day! "
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def create_preference_seed(preference):
    # save to dynamodb
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('User')
    response = table.put_item(Item={'UserId': 'one', 'Seed': preference})
    return {"preference": preference}


def set_preference_seed(intent, session):
    """ Sets the meal in the session and prepares the speech to reply to the
    user.
    """

    card_title = intent['name']
    session_attributes = {}
    should_end_session = False

    if 'Pref' in intent['slots']:
        preference = intent['slots']['Pref']['value']
        session_attributes = create_preference_seed(preference)
        speech_output = "I now know you are a " + preference
        reprompt_text = "You can reset your preference by saying, " \
                        "I am a vegetarian"
    else:
        speech_output = "I'm not sure what your preference is. " \
                        "Please try again."
        reprompt_text = "I'm not sure what your preference is. " \
                        "You can tell me your preference meal by saying, " \
                        "I am a vegetarian."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def get_seed_from_db(intent, session):
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('User')
    response = table.get_item(Key={'UserId': 'one'})

    session_attributes = {}
    speech_output = ''
    reprompt_text = "Sorry, seed is not set up yet. Please say something like " \
    "I am a vegetarian, to set your seed to be a vegetarian."
    try:
        response = table.get_item(Key={'UserId': 'one'})
    except ClientError as e:
        print(e.response['Error']['Message'])
        speech_output = "Seed is not set up yet. "
    else:
        item = response['Item']
        print("GetItem successful")
        if item['Seed']:
            speech_output = "You are a " + item['Seed']
        else:
            speech_output = "Seed not set up yet for user " + item['UserId']

    return build_response(session_attributes, build_speechlet_response(intent['name'], speech_output, reprompt_text, True))

def get_meal_from_session(intent, session):
    session_attributes = {}
    reprompt_text = None

#    if session.get('attributes', {}) and "preference" in session.get('attributes', {}):
#        preference = session['attributes']['preference']
#        speech_output = "You are a " + preference + \
#                        ". Goodbye."
#        should_end_session = True
#    else:

    # read from db...
    meals = ['chicken salad', 'barbeque wings', 'ahi tuna roll', 'vegetarian burger']
    meal = random.choice(meals)
    #speech_output = "I'm not sure what your favorite meal is. " \
    #               "You can say, my favorite meal is barbeque wings."
    speech_output = "You should eat {meal}.".format(meal=meal)
    should_end_session = False

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    return build_response(session_attributes, build_speechlet_response(
        intent['name'], speech_output, reprompt_text, should_end_session))


# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name == "MyMealSeed":
        return set_preference_seed(intent, session)
    elif intent_name == "WhatsMyMealSeed":
        return get_seed_from_db(intent, session)
    elif intent_name == "WhatsMyMealIntent":
        return get_meal_from_session(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
