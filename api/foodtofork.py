import requests

API_KEY = '7d517eb94b0168f5d27e237dad1c3f84'
base_url = 'http://food2fork.com/api/'

def search(in_keyword):
    """
    Takes in a keyword (string) and searches the Food2Fork api for recipes
    that match this keyword
    
    Throws ValueError exception to handle bad user input
    :param in_keyword: 
    :return: json object with all of the top level recipe information
    """
    return requests.get(base_url + 'search?key={api_key}&q={keyword}'.format(api_key=API_KEY, keyword=in_keyword))

def get(in_rId):
    """
    Takes in a recipeId from, which can be found from our search function,
    and returns more granular data on that recipe, including ingredients
    :param rId: 
    :return: json object of an individual recipe
    """
    return requests.get(base_url + 'get?key={api_key}&rId={rId}'.format(api_key=API_KEY, rId=in_rId))


res = search('chicken')
print(res)
