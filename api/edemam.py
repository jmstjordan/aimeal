import requests

APP_ID = 'f46ac583'
APP_KEY = 'dc6e1e016bef181bfff358efff43f888'
base_url = 'https://api.edamam.com/'

def search(search_type, param, **kwargs):
    """
    Takes in a search type ('r' or 'q'), a keyword (string),
    and a keyword list of optional parameters to filter the search
    based on https://developer.edamam.com/edamam-docs-recipe-api
    Searches the Edamam api for recipes that match this keyword
    and parameters

    Throws ValueError exception to handle bad user input
    
    :param param: 
    :return: json object with all of the top level recipe information
    """
    if search_type is not 'r' and search_type is not 'q':
        raise ValueError('incorrect search type, must be r or q')

    # TODO: Fix this hack, the # in our searches recipeId URL needs to be
    # TODO: replaces for the r function to work
    if search_type is 'r' and '#recipe' in param:
        param = param.replace('#', '%23')

    params = ''
    for a in kwargs:
        params += '&{param_name}={param_value}'.format(param_name=a, param_value=kwargs[a])

    return requests.get(base_url + 'search?{search_type}={param}&app_id={app_id}&app_key={app_key}{params}'
                        .format(search_type=search_type, param=param, app_id=APP_ID, app_key=APP_KEY, params=params))

#res = search('q', 'beets', diet='balanced')
#res_with_id = search('r', 'http://www.edamam.com/ontologies/edamam.owl#recipe_4727f12960724a94bfff405768eb5c39')
#print(res.text)
