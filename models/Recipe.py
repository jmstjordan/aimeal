
class Recipe(object):
    """
    Object representing our recipe. Each recipe pulled from Edemam 
    and stored as a Recipe object
    """
    def __init__(self, edemam_id, name, ingredients, instructions, diet_labels,
                 health_labels, cautions, calories, nutrients, image):

        # URL-like ID from edemam
        self.edemam_id = edemam_id
        self.name = name

        # list of ingredients
        self.ingredients = ingredients

        # link to instructions
        self.instructions = instructions

        # list of diet labels
        self.diet_labels = diet_labels

        # list of health labels
        self.health_labels = health_labels

        # list of cautions
        self.cautions = cautions
        self.calories = calories

        # dict of dicts that have nutrient information like fat, sugar, etc.
        self.nutrients = nutrients

        # link to img
        self.image = image



